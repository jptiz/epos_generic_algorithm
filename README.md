Genetic Algorithm for EPOS Device
=================================

About
-----

This project aims to build "the best" EPOS configuration by setting different
compile settings and evaluating its success/fail through pre-defined criterias
and a genetic algorithm.

Currently it's using [DEAP lib](https://github.com/DEAP/deap), as it works, is
in Python and it's simple lib that can be used in python and doesn't forces
you to get away from regular python programming style just to use the lib,
which also allows the use of [matplotlib](http://matplotlib.org/) for graphics
generation.

Ideas
-----

Our basic idea is to have some set of parameters from each `*_traits.h`, for
example:

```
record TraitParams:
  heap_size,
  stack_size,
  max_threads
;
```

Evaluation Function
-------------------

The evaluation function should be as something as this:

```
function Evaluate(gene):
  grade = 0
  code = generate_header(gene.heap_size, gene.stack_size, gene.max_threads)
  compilation_result = compile(code)
  if (compilation_result.success):
    grade += 1000
  ;
  grade += 1000 - 20 * compilation_result.warning_count
  grade += 1000 - 500 * compilation_result.error_count
  run_result = run(compilation_result.output)
  if (run_result.success):
    grade += 500
  ;
  grade += 10000 - run_result.milliseconds
;
```

Future evaluations may consider page-fault count, cache miss-rate and other
things, but basically these will be denounced by total runtime.