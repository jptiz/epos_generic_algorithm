'''EPOS Genetic Algorithm features for compilation and execution tasks'''
import shlex
import time
from re import findall
from random import randint
from subprocess import check_call, Popen, PIPE, CalledProcessError
from time_limit import time_limit, TimeoutException


TIME_LIMIT = 300 # in seconds


HEADER_BASE = open('default_traits.h', 'r').read()


def current_time():
    '''
        Gets current system time.
    '''
    return int(round(time.time() * 1000))


def random_trait():
    '''
        Generates a random trait.
    '''
    if random_trait.counter == 0:
        trait = randint(1, 16 * 1024)
    elif random_trait.counter == 1:
        trait = randint(1, 16 * 1024 * 1024)
    elif random_trait.counter == 2:
        trait = randint(1, 16)

    random_trait.counter = (random_trait.counter+1) % 3
    return trait
random_trait.counter = 0


class CompilationResult:
    '''
        Encapsulates compilation results
    '''
    def __init__(self):
        self.success = False
        self.warning_count = 0
        self.error_count = 0
        self.output = ''
        self.error = ''
        self.ellapsed_time = 0


def clean():
    '''
        Cleans EPOS generated files.
    '''
    begin = current_time()
    args = shlex.split('make -C epos/ APPLICATION=fork veryclean')
    p = Popen(args, stdin=None, stdout=PIPE, stderr=PIPE)
    _, _ = p.communicate()
    return current_time() - begin


def make_header(trait):
    '''
        Generates a traits header.
    '''
    begin = current_time()
    header = HEADER_BASE.format(trait[0], trait[1], trait[2])
    return header, current_time() - begin


def compile_trait(header):
    '''
        Compiles the application with given traits header.

        header (str): Traits header.
    '''
    result = CompilationResult()

    f = open('epos/app/fork_traits.h', 'w')
    f.write(header)
    f.close()

    args = shlex.split('make -C epos/ APPLICATION=fork')
    begin = current_time()
    p = Popen(args, stdin=None, stdout=PIPE, stderr=PIPE)
    output, error = p.communicate()
    result.ellapsed_time = current_time() - begin

    output = output.decode('utf-8')
    error = error.decode('utf-8')

    result.error_count = len(
        findall('[^:]+:[0-9*]+:[0-9*]+: error:', error))
    result.warning_count = len(
        findall('[^:]+:[0-9*]+:[0-9*]+: warning:', error))
    result.output = output
    result.error = error
    result.success = len(error) == 0

    if len(error) != 0:
        print('error:\n{}\n'.format(error))
    return result


def run():
    '''
        Runs generated application.
    '''
    args = shlex.split('make -C epos/ APPLICATION=fork run')
    try:
        begin = current_time()
        try:
            time_limit(TIME_LIMIT)
            check_call(args, stdin=None, stdout=PIPE, stderr=PIPE)
        except TimeoutException:
            print('Time limit reached.')

        ellapsed_time = current_time() - begin
        success = True

    except CalledProcessError as e:
        success = False
        ellapsed_time = 0
        print(e.with_traceback())

    return success, ellapsed_time


def data_segment_size():
    '''
        Gets generated data segment size.
    '''
    try:
        run_out = open('epos/img/fork.out', 'r').read()
    except FileNotFoundError:
        return 0
    size = findall(r"APP code:.*data: (\d+)", run_out)
    if len(size) == 0:
        return 0
    return int(size[0])


def evaluate(trait):
    '''
        Evaluates a trait.

        trait (tuple): Trait parameters: Stack size, heap size and maxmimum
                       threads.
    '''
    grade = 0
    total_time = 0

    print('-----------------------------------------')
    print('PROCESSING TRAIT {}'.format(trait))
    print('CLEANING...')
    clean_time = clean()
    total_time += clean_time

    print('DONE! ({}ms) GENERATING HEADER...'.format(clean_time))
    header, ellapsed = make_header(trait)
    total_time += ellapsed

    print('DONE! ({}ms) COMPILING...'.format(ellapsed))
    result = compile_trait(header)
    grade -= 20 * result.warning_count
    grade -= 500 * result.error_count
    total_time += result.ellapsed_time

    if result.success:
        print('COMPILATION SUCCESSFUL! ({}ms) RUNNING...'.format(
            result.ellapsed_time))
        grade += 100000
        success, ellapsed = run()
        total_time += ellapsed
        if success:
            print('RUN SUCCESSFUL! ({}ms)'.format(ellapsed))
            grade += 500
            grade -= ellapsed + (data_segment_size()//4096)

    print('GRADE: {} ({}ms)'.format(grade, total_time))
    return grade,
