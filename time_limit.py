'''Contains functions for time-limiting scopes.'''
import signal

class TimeoutException(Exception):
    '''Raised when a function call take too long to execute.'''
    pass

def time_limit(seconds):
    '''
        Ensures scope doesn't take longer than the amount of *seconds* to execute.
    '''
    def time_limiter(_, __):
        '''
            Raises exception for time-out handling
        '''
        raise TimeoutException('Timed out!')

    signal.signal(signal.SIGALRM, time_limiter)
    signal.alarm(seconds)
