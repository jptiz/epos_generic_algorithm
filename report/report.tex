\documentclass{article}
\usepackage{algorithm2e}
\usepackage{graphicx}
\graphicspath{{img/}}
\usepackage{listings}
\usepackage{makeidx}
\usepackage{polyglossia}
\setmainlanguage{portuges}

\title{Sistema Computacional \\
		\textsc{Modelo Evolucionário - EPOS}}
\author{João Paulo Taylor Ienczak Zanette \\
        Otto Menegasso Pires}


\renewcommand{\figurename}{Figura}

\renewcommand{\contentsname}{Índice}
\renewcommand{\refname}{Referências}

\begin{document}

\maketitle

\tableofcontents
\newpage

\section{Introdução}

\par Esse projeto, desenvolvido para a disciplina de Sistemas Operacionais I, tem como objetivo criar um Modelo Evolucionário do EPOS\@. Um modelo evolucionário é um vetor de parâmetros otimizados através de um algoritmo evolucionário.
\par Para se criar o modelo foi usado o framework DEAP (Distributed Evolutionary Algorithms in Python)\cite{DEAP_JMLR2012} em sua versão 1.0.2, escolhido por sua praticidade e por preferência dos autores. Para construir os gráficos de análise foi usado a biblioteca MatPlotLib\cite{Hunter:2007}.

\subsection{Algoritmos Evolucionários}

\par De maneira simplificada, algoritmos evolucionários são modelos computacionais inspirados na ideia biológica de Evolução. Nesses modelos cada variável associada ao problema é mapeada em um Gene, que são usados para compor Indivíduos. Os indivíduos representam, de maneira codificada, uma possível solução para o problema. A População é o conjunto de indivíduos que são alterados a cada Geração.

\par Após gerar uma população inicial, o algoritmo calcula a próxima geração a partir do cruzamento e da mutação dos genes da população anterior. Existem diversas fórmulas disponíveis no DEAP para efetuar o cruzamento de genes e a mutação.

\par Após um número suficiente de gerações, escolhido pelo desenvolvedor, espera-se ter encontrado a solução ótima do problema, sendo essa solução codificada como o indivíduo mais apto encontrado durante a execução do algoritmo.

\subsection{EPOS}

\par \textit{"The EPOS Project (Embedded Parallel Operating System) aims at automating the development of embedded systems so that developers can concentrate on what really matters: their applications. EPOS relies on the Application-Driven Embedded System Design (ADESD) method to guide the development of both software and hardware components that can be automatically adapted to fulfill the requirements of particular applications. EPOS features a set of tools to support developers in selecting, configuring, and plugging components into its application-specific framework. The combination of methodology, components, frameworks, and tools enable the automatic generation of an application-specific embedded system instances."}[Retirado de https://epos.lisha.ufsc.br/HomePage]
\par O EPOS é um framework para desenvolvimento de sistemas embarcados desenvolvido pelo Laboratório de Integração Software Hardware (LISHA) da UFSC\@. Para criar o modelo evolucionário foi alterado o arquivo {\it traits} de uma aplicação-exemplo disponível com o EPOS, para que ele aceitasse os parâmetros alterados pelo algoritmo genético.

\subsection{Objetivos}

\par Desenvolver um modelo evolucionário que gere a melhor configuração para uma aplicação EPOS dados determinados critérios. Os parâmetros escolhidos para compor o gene foram: Tamanho da Pilha ({\it Stack Size}), Tamanho da Heap ({\it Heap Size}) e o Número Máximo de Threads da aplicação.

\subsection{Requisitos}

\par O sistema possui como requisitos: a criação de uma função de geração de genes (indivíduos), bem como uma função de avaliação deles, dados determinados parâmetros, e utilizá-las para criar um algoritmo genético que possa adequar parâmetros a uma aplicação EPOS.

\newpage

\section{Desenvolvimento}

\par O desenvolvimento foi separado em dois módulos: um para gerência do algoritmo genético e comunicação com a biblioteca e outro para definir as propriedades dos genes (funções de criação e avaliação).

\subsection{Módulo Gerenciador}

\par O gerenciador possui apenas uma função principal que invoca as funções das bibliotecas DEAP, matplotlib (para formação de gráficos), entre outras. A base do algoritmo descrito pelo gerenciador é:

\begin{algorithm}
    \SetKwInOut{Input}{input}
    \SetKwInOut{Output}{output}
    \SetKw{Let}{let}

    \Input{The number of generations N, the population size S}
    \Output{A trained population P}

    \Let{P be a randomly initialized population}

    \For{$g \leftarrow 1$ \KwTo $N$}{
        $variate(P)$\;
        \For{i {\bf in} P}{
            $i.fitness \leftarrow evaluate(i)$\;
        }
        $P \leftarrow select(P)$\;
    }
\end{algorithm}

A função de variação ($variate$) corresponde a aplicar algoritmos de cruzamento e mutação em todas as gerações, definida pela chamada $varAnd$ ({\it {\bf variate} with crossover {\bf and} mutation}) do DEAP. A função seleção de uma determinada população também é definida pela biblioteca.

\subsection{Modulo Genético}

\par No módulo genético estão presentes as funções de geração ($make\_trait$) e avaliação ($evaluate$), que por sua vez necessitam de outras funções. Cada passo é encapsulado em uma classe contendo dados de seus resultados.

\subsubsection{Função de geração}

A função de geração é definida da forma:

\begin{algorithm}
    \SetKwInOut{Output}{output}

    \Output{A set of traits T}

    $T[1] \leftarrow randomInteger(0$, $16 \times 1024)$\;
    $T[2] \leftarrow randomInteger(0$, $16 \times 1024 \times 1024)$\;
    $T[3] \leftarrow randomInteger(0$, $16)$\;
\end{algorithm}

Em que $T[1]$ é o Tamanho da Pilha, $T[2]$ é o Tamanho da Heap e $T[3]$ é o número máximo de Threads.

\subsubsection{Função de Avaliação}

A função de avaliação é já mais complicada e envolve diretamente o funcionamento do EPOS. Os dois critérios mais agravantes da nota são:
\begin{itemize}
    \item Compilar com sucesso (bônus de 100000 pontos, com penalidades para cada {\it warning} ocorrido);
    \item Ser o mais rápido possível (penalidade igual ao tempo decorrido de execução em milisegundos).
\end{itemize}

\begin{algorithm}
    \SetKwInOut{Input}{input}
    \SetKwInOut{Output}{output}
    \SetKw{Let}{let}

    \Input{A trait T}
    \Output{T's grade G}

    $G \leftarrow 0$\;
    $clean\_last\_execution()$\;
    $header \leftarrow make\_header(T)$\;
    $compile\_results \leftarrow compile(header)$\;
    $G \leftarrow G - 20 \times compile\_results.warning\_count$\;
    $G \leftarrow G - 500 \times compile\_results.error\_count$\;
    \If{$compile\_results.success$}{
        $G \leftarrow G + 100000$\;
        $run\_results \leftarrow run()$\;
        \If{$run\_results.success$}{
            $G \leftarrow G - run\_results.ellapsed_time$
        }
        \Else{
            $G \leftarrow G - 50000$
        }
    }
\end{algorithm}

\newpage

\section{Resultados}

\subsection{Considerações Iniciais}

\par A escolha de uma biblioteca implica testá-la. Assim, um algoritmo genético simples foi utilizado, em que o objetivo era que cada indivíduo formasse o texto "Hello, World!", com os exatos mesmos caracteres, o que resultou nas seguintes estatísticas:

\begin{itemize}
    \item População: 200
    \item Gerações: 125
    \item Tempo de execução: 2065ms;
    \item Gerações até atingir a nota máxima: 18
\end{itemize}

\par Percebe-se, pela Figura \ref{fig:stats_hello} que a biblioteca teve um bom desempenho e trouxe rapidamente ao menos um indivíduo com a característica desejada, mantendo a média de indivíduos convergindo para próximo da nota máxima.

\begin{figure}[ht]
  \caption{Avaliação dos genes do problema "Hello, World!". Números maiores representam melhores aptidões.}
  \label{fig:stats_hello}
  \centering
    \includegraphics[width=1\textwidth]{stats_hello}
\end{figure}

\subsection{Plataforma}

\begin{itemize}
    \item Tipo: Notebook
    \item Processador: Intel i5-4210U 1.7 GHz
    \item Memória Principal: 8GB
    \item Memória Secundária: 128GB (SSD)
    \item Sistema Operacional: Arch Linux 64-bits.
\end{itemize}

\subsection{Parâmetros}

\par Levou-se em conta uma população de {\bf 23 indivíduos} sendo variada e avaliada por {\bf 40 gerações}. Tais parâmetros forma baseados em uma média (calculada com algumas execuções prévias do algoritmo) de $25s$ de tempo de avaliação de um indivíduo, considerou-se um teto de $30s$ por indivíduo sendo variados por no máximo um tempo de execução de 8 horas.

\subsection{Estatísticas}

\par Através dos métodos disponíveis nas bibliotecas DEAP, NumPy\cite{SciPy} e MatPlotLib foi possível gerar estatísticas a respeito do algoritmo em questão, sendo escolhidas as estatísticas de notas dadas aos genes a cada geração, sendo elas: nota máxima, nota mínima, e média entre ambas em cada população.

\par É possível notar uma certa convergência das notas, uma vez que começam com valores bastante discrepantes de uma geração a outra e pouco a pouco essa diferença se torna menos brusca.

\begin{figure}[ht]
  \caption{Performance dos programas avaliados. Valores maiores indicam melhor performance.}
  \centering
    \includegraphics[width=1\textwidth]{stats_epos}
\end{figure}

\section{Conclusões}

\par A primeira observação é ser feita é o tempo de processamento/avaliação dos indivíduos. A mutação/cruzamento de indivíduos é um processo extremamente rápido e trivial, uma vez que envolve apenas inteiros, deixando como gargalo as operações em disco, sendo elas:

\begin{itemize}
    \item Limpeza da última compilação - necessária devido à forma como foi projetado o EPOS -;
    \item A compilação em si - que possui trabalho extra por conta de não se poder aproveitar os artefatos já compilados anteriormente, devido ao item anterior;
    \item Execução do software, que precisa ser emulado para a arquitetura-destino.
\end{itemize}

\par E, portanto, classifica-se o algoritmo como {\bf IO-Bound}, denunciado por outro teste, de mesmos parâmetros, executado em uma máquina com um HDD como único meio de armazenamento elevando o tempo de processamento do primeiro e segundo item de, respectivamente, 4s e 17s (SSD) para 15s e 40s (HDD). Portanto algumas soluções poderiam ser adotadas, tais como a utilização do escalonador BFS\cite{bfs:2016}, que apresenta melhor desempenho em soluções IO-Bound.

\newpage

\bibliographystyle{unsrt}
\bibliography{report}

\end{document}
