''' Genetic algorithm for generating a 'Hello, World!' string'''
import epos
from epos import random_trait
import numpy
import matplotlib.pyplot as plot
from deap import creator
from deap import base
from deap import tools
from deap import algorithms

POP_SIZE = 40
GEN = 135


def main():
    '''Runs genetic algorithm that converges to the best EPOS '''

    creator.create('FitnessMax', base.Fitness, weights=(1.0,))
    creator.create('Individual', list, fitness=creator.FitnessMax)

    toolbox = base.Toolbox()

    toolbox.register('individual', tools.initRepeat, creator.Individual,
                     random_trait, n=3)
    toolbox.register('pop', tools.initRepeat, list, toolbox.individual)

    toolbox.register('evaluate', epos.evaluate)
    toolbox.register('mate', tools.cxTwoPoint)
    toolbox.register('mutate', tools.mutUniformInt, indpb=0.05, low=(16, 16, 16),
                     up=(16*1024, 16*1024*1024, 16))
    toolbox.register('select', tools.selTournament, tournsize=1)

    pop = toolbox.pop(n=POP_SIZE)

    stats = tools.Statistics(key=lambda ind: ind.fitness.values)
    stats.register('avg', numpy.mean)
    stats.register('min', numpy.min)
    stats.register('max', numpy.max)

    logbook = tools.Logbook()
    logbook.header = ['gen', 'evals'] + stats.fields

    for g in range(GEN):
        offspring = algorithms.varAnd(pop, toolbox, cxpb=.5, mutpb=.1)

        for ind in offspring:
            for j, char in enumerate(ind):
                if char == 'False' or char == 'True':
                    ind[j] = random_trait()[j]

        fits = toolbox.map(toolbox.evaluate, offspring)
        for fit, ind in zip(fits, offspring):
            ind.fitness.values = fit
        record = stats.compile(offspring)
        logbook.record(gen=g, evals=len(offspring), **record)

        print(logbook.stream)
        print('end of generation {}'.format(g))

        pop = toolbox.select(offspring, k=len(pop))

    avg, min_, max_ = logbook.select('avg', 'min', 'max')
    plot.plot(avg, label='Avg')
    plot.plot(min_, label='Min')
    plot.plot(max_, label='Max')
    plot.legend(loc='lower right')
    plot.axis([0, GEN-1, min(min_), max(max_)])
    plot.xlabel('Generations')
    plot.ylabel('Grade')
    plot.savefig('graph.png')


if __name__ == '__main__':
    main()
